import Banner from '../components/Banner';
export default function Error() {
  const data = {
        title: "Page not found",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to homepage"
    }  
    return (
 <Banner data={data} />
    )
}

